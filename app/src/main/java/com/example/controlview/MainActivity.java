package com.example.controlview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //aqui se agregan los objetos

    private TextView tv1;
    private ListView lv1;

    private String nombre [] = {"uriel", "Vanessa", "Emily"};
    private String edades [] = {"24", "22", "18"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //relacion entre la parte grafica y la logica

        tv1 = (TextView)findViewById(R.id.tv1);
        lv1 = (ListView)findViewById(R.id.lv1);

        ArrayAdapter <String> adapter = new ArrayAdapter<String>(this, R.layout.list_item_uriel, nombre);
        //mostrar lo que se configura en adapter
        lv1.setAdapter(adapter);

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tv1.setText("la edad de " + lv1.getItemAtPosition(position) + " es de " + edades [position] + " años ");

            }
        });


    }
}
